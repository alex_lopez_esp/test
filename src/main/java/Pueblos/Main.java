package Pueblos;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) {
        filesOrigen();


    }

    static void filesOrigen() {
        ListaProvincias listaProvincias = new ListaProvincias();

        List<Pueblos> pueblos = new ArrayList<Pueblos>();
        Pueblos pueblo = new Pueblos(1,"jUAN", 22, 21);
        Pueblos pueblo1 = new Pueblos(2,"asdnt", 344444444, 12333);
        Pueblos pueblo2 = new Pueblos(3,"dinamarca", 344444444, 12333);
        pueblos.add(pueblo);
        pueblos.add(pueblo1);
        Provincia provincia = new Provincia("alccante", 213131, 565666, pueblos);
        pueblos.add(pueblo2);
        Provincia provincia1 = new Provincia("Madrid", 123331, 231, pueblos);
        List<Provincia> totalProvincias = new ArrayList<>();
        totalProvincias.add(provincia);
        totalProvincias.add(provincia1);
        listaProvincias.setProvincias(totalProvincias);
        transformartoXML(listaProvincias);

    }

    static void transformartoXML(ListaProvincias listaProvincias) {
        try {
            // Creamos el contexto JAXB basado en Biblioteca.class
            JAXBContext context = JAXBContext.newInstance(ListaProvincias.class);

            // Instanciamos un marshaller o serializador
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");               // valor por defecto

            m.marshal(listaProvincias, System.out);

            // Escritura del fichero XML
            try (FileWriter fw = new FileWriter("Provincias.xml")) {
                m.marshal(listaProvincias, fw);
            } catch (IOException ex) {
                System.err.println("Problemas durante la escritura del fichero..." + ex.getMessage());
            }

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}