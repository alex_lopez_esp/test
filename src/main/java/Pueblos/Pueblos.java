package Pueblos;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"num","nombrePueblo","poblacion","altitud"})
public class Pueblos {
    int num;
    String nombrePueblo;
    long poblacion;
    long altitud;


    public Pueblos() {}

    public Pueblos(int num,String nombrePueblo, long poblacion, long altitud) {
        this.num=num;
        this.nombrePueblo = nombrePueblo;
        this.poblacion = poblacion;
        this.altitud = altitud;
    }
    @XmlAttribute
    public int getNum() { return num; }
    public void setNum(int num) { this.num = num; }
    @XmlElement(name = "nombrePueblo")
    public String getNombrePueblo() { return nombrePueblo; }
    public void setNombrePueblo(String nombrePueblo) { this.nombrePueblo = nombrePueblo; }
    @XmlElement(name = "poblacion")
    public long getPoblacion() { return poblacion; }
    public void setPoblacion(long poblacion) { this.poblacion = poblacion; }
    @XmlElement(name = "altitud")
    public long getAltitud() { return altitud; }
    public void setAltitud(long altitud) { this.altitud = altitud; }
}
