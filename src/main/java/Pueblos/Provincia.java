package Pueblos;


import javax.xml.bind.annotation.XmlElement;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;
@XmlType(propOrder = {"nombre","superficie","poblacion","pueblos"})
public class Provincia implements Serializable {
    String nombre;
    long superficie;
    long poblacion;
    List<Pueblos> pueblos;

    public Provincia() {}

    public Provincia(String nombre, long superficie, long poblacion, List<Pueblos> pueblos) {
        this.nombre = nombre;
        this.superficie = superficie;
        this.poblacion = poblacion;
        this.pueblos = pueblos;
    }

    @XmlElement(name = "nombre-pr")
    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
    @XmlElement(name = "superficie-pr")
    public long getSuperficie() { return superficie; }
    public void setSuperficie(long superficie) { this.superficie = superficie; }
    @XmlElement(name = "poblacion-pr")
    public long getPoblacion() { return poblacion; }
    public void setPoblacion(long poblacion) { this.poblacion = poblacion; }
    @XmlElementWrapper(name = "pueblos")
    public List<Pueblos> getPueblos() { return pueblos; }
    public void setPueblos(List<Pueblos> pueblos) { this.pueblos = pueblos; }
}
