package Pueblos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
@XmlRootElement(name = "provincias")
public class ListaProvincias implements Serializable {
    List<Provincia> provincias ;


    public ListaProvincias() {}

    @XmlElementWrapper(name = "provincias")
    public List<Provincia> getProvincias() { return provincias; }
    public void setProvincias(List<Provincia> provincias) { this.provincias = provincias; }
}
