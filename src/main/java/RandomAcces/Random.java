package RandomAcces;

import java.io.*;
import java.util.ArrayList;

public class Random {
    public static void main(String[] args) {
        RandomAccessFile fichero1;
        RandomAccessFile fichero2;
        RandomAccessFile fichero3;
        try {
            fichero1 = new RandomAccessFile("hola.dat", "rw");
            fichero2 = new RandomAccessFile("hola2.dat", "rw");
            fichero3 = new RandomAccessFile("hola3.dat", "rw");
            fichero1.writeShort(11);
            fichero1.writeUTF("mesa");
            fichero1.writeFloat(104.50F);
            //-----------------------------
            fichero1.writeShort(12);
            fichero1.writeUTF("silla");
            fichero1.writeFloat(80.30F);
            //-----------------------------
            fichero1.writeShort(13);
            fichero1.writeUTF("sofa");
            fichero1.writeFloat(150.80F);

            //------------------------------------------------------
            fichero2.writeShort(21);
            fichero2.writeUTF("disco");
            fichero2.writeFloat(80.50f);
            //-------------------------------
            fichero2.writeShort(22);
            fichero2.writeUTF("movil");
            fichero2.writeFloat(250.10f);
            //------------------------------------------------------
            fichero3.writeShort(31);
            fichero3.writeUTF("monitor");
            fichero3.writeFloat(120.30f);
            //-------------------------------
            fichero3.writeShort(32);
            fichero3.writeUTF("router");
            fichero3.writeFloat(55.50f);

            mezclar(fichero1,fichero2,fichero3);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static void  mezclar(RandomAccessFile fichero1, RandomAccessFile fichero2, RandomAccessFile fichero3){


        try {
            ArrayList<RandomAccessFile> ficheros = new ArrayList<>();
            ficheros.add(fichero1);
            ficheros.add(fichero2);
            ficheros.add(fichero3);
            File ficheross = new File("resultado.dat");

            BufferedWriter bw=new BufferedWriter(new FileWriter(ficheross));
            int calculo = 0;
            long posicionFichero1 = 0;
            long posicionFichero2 = 0;
            long posicionFichero3 = 0;
            StringBuilder sb=new StringBuilder();
            StringBuilder sb1=new StringBuilder();
            StringBuilder sb2=new StringBuilder();
            for (int i = 0; i < ficheros.size(); i++) {
                for (int j = 0; j < ficheros.get(i).length() / 10; j++) {
                    calculo = calculo + 1;
                }
            }

            try {
                for (int x = 0; x < calculo; x++) {
                    if (x == 0 || x == 3 || x == 6) {
                        fichero1.seek(posicionFichero1);
                        bw.write(String.valueOf(sb.append(fichero1.readShort()).append("-").append(fichero1.readUTF()).append("-").append(fichero1.readFloat()).append("\n")));
                        posicionFichero1 = fichero1.getFilePointer();
                        sb.setLength(0);

                    } else if (x == 1 || x == 4) {
                        fichero2.seek(posicionFichero2);
                        bw.write(String.valueOf(sb1.append(fichero2.readShort()).append("-").append(fichero2.readUTF()).append("-").append(fichero2.readFloat()).append("\n")));
                        posicionFichero2 = fichero2.getFilePointer();
                        sb1.setLength(0);

                    } else {
                        fichero3.seek(posicionFichero3);
                        bw.write(String.valueOf(sb2.append(fichero3.readShort()).append("-").append(fichero3.readUTF()).append("-").append(fichero3.readFloat()).append("\n")));
                        posicionFichero3 = fichero3.getFilePointer();
                        sb2.setLength(0);

                    }

                }
                bw.close();

            } catch (IOException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
            System.out.println(calculo);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}



