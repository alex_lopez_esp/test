package Clientes;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "clientes")
public class ListaClientes implements Serializable {
    List<Cliente> clientes;

    public ListaClientes() {}

    @XmlElement(name = "clientes")
    public List<Cliente> getClientes() { return clientes; }
    public void setClientes(List<Cliente> clientes) { this.clientes = clientes; }
}
