package Clientes;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;

import java.util.ArrayList;
import java.util.List;

public class Parte1 {
    public static void main(String[] args) {

        filesOrigen();
        transformaOosToXml(new File("origen.dat"), new File("destino.xml"));
    }

    static void filesOrigen() {
        File origen = new File("origen.dat");
        ArrayList<String> telefonos = new ArrayList<>();
        telefonos.add("345345345435");
        Cliente cliente = new Cliente(1, "Manolo", "Manco", telefonos, "asdadsads@hotmail.com", 1231, 22);
        ArrayList<String> telefonos2 = new ArrayList<>();
        telefonos2.add("345345345435");
        telefonos2.add("123123123");
        Cliente cliente2 = new Cliente(2, "Antonio", "Francias", telefonos2, "asdasdasj@hotmail.com", 1234, 44);

        List<Cliente> lista = new <Cliente>ArrayList();
        lista.add(cliente);
        lista.add(cliente2);
        ListaClientes listaClientes=new ListaClientes();
        listaClientes.setClientes(lista);

        FileOutputStream fos;

        try {
            fos = new FileOutputStream(origen);

            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(listaClientes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void transformaOosToXml(File origin, File destino) {
        FileInputStream fis;
        ObjectInputStream oos;
        ListaClientes listaClientes = null;
        try {


            fis = new FileInputStream(origin);
            oos = new ObjectInputStream(fis);
            listaClientes =(ListaClientes)oos.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            JAXBContext context = JAXBContext.newInstance(ListaClientes.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

            FileWriter fw = new FileWriter(destino);
            m.marshal(listaClientes, fw);
        } catch (IOException | JAXBException  e) {
            e.printStackTrace();
        }

    }
}
