package Clientes;

import javax.xml.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@XmlType(propOrder = {"id","nombre","apellidos","telefonos"})
public class Cliente implements Serializable {
    int id;
    String nombre;
    String apellidos;
    List<String> telefonos;
    String email;
    float saldo;
    double auxiliar;


    public Cliente() {}
    public Cliente(int id, String nombre, String apellidos, ArrayList<String> telefonos, String email, float saldo, double auxiliar) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.telefonos = telefonos;
        this.email = email;
        this.saldo = saldo;
        this.auxiliar = auxiliar;
    }

    @XmlAttribute
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    @XmlElement(name = "nombre")
    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
    @XmlElement(name = "apellidos")
    public String getApellidos() { return apellidos; }
    public void setApellidos(String apellidos) { this.apellidos = apellidos; }
    @XmlElementWrapper(name = "telefonos")
    public List<String> getTelefonos() { return telefonos; }
    public void setTelefonos(List<String> telefonos) { this.telefonos = telefonos; }
    @XmlTransient
    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }
    @XmlTransient
    public float getSaldo() { return saldo; }
    public void setSaldo(float saldo) { this.saldo = saldo; }
    @XmlTransient
    public double getAuxiliar() { return auxiliar; }
    public void setAuxiliar(double auxiliar) { this.auxiliar = auxiliar; }
}
